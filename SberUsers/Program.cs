﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace SberUsers
{
    class Program
    {
        const string connectionString = "Host=localhost:8000;Username=postgres;Password=23412M;Database=postgres";

        static void Main(string[] args)
        {
            GetAllTables();

            Console.WriteLine("Press Y to insert");
            if (Console.ReadLine() == "Y")
            {
                Console.WriteLine("Enter the table Name:");
                InsertToTable(Console.ReadLine());
            }
            else return;
            
            Console.ReadKey();
        }

        /// <summary>
        /// Создание нового подключения
        /// </summary>
        /// <returns></returns>
        static NpgsqlConnection CreateConnection()
        {
            var connection = new NpgsqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Подключение к БД и получение всех таблиц
        /// </summary>
        static void GetAllTables()
        {
            List<string> tableNames = new List<string>();

            using var newConnection = CreateConnection();
            
            var sql = "SELECT * FROM information_schema.tables WHERE table_schema = 'public';";

            using var cmd = new NpgsqlCommand(sql, newConnection);

            using var reader = cmd.ExecuteReader();

            Console.WriteLine("Tables in DB:");
            while (reader.Read())
            {
                var tableName = reader.GetValue(2).ToString();
                Console.WriteLine("\t{0}", tableName);
                tableNames.Add(tableName);
            }
            reader.Close();
            GetAllRows(tableNames, newConnection);
        }

        /// <summary>
        /// Получить все записи во всех таблицах
        /// </summary>
        /// <param name="tableNames">Список названия таблиц</param>
        static void GetAllRows(List<string> tableNames, NpgsqlConnection conn)
        {
            var queryConstructor = new QueryConstructor();
            var queries = queryConstructor.SetAllRowsQuery(tableNames);
            int index = 0;

            queries.ForEach(query => {
                using var cmd = new NpgsqlCommand(query, conn);
                    using var reader = cmd.ExecuteReader();
                    index++;

                    while (reader.Read())
                    {
                        Console.WriteLine(tableNames[index-1]);
                        for (int i = 0; i < reader.FieldCount; i++) {
                                Console.Write(reader.GetValue(i) + "  ");
                        }
                    Console.WriteLine();
                    }
            });

            conn.Close();
        }

        /// <summary>
        /// Получить заголовки столбцов
        /// </summary>
        /// <param name="tableName">Название таблицы</param>
        /// <returns>Список элементов для запроса</returns>
        static List<UserOptions> GetTableHeaders(string tableName)
        {
            var conn = CreateConnection();
            var queryConstructor = new QueryConstructor();
            var query = queryConstructor.SetAllRowsQuery(tableName);

            using var cmd = new NpgsqlCommand(query, conn);
            using var reader = cmd.ExecuteReader();

            int index = 0;
            List<UserOptions> options = new List<UserOptions>();

            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount-1; i++)
                {
                    if (index < reader.FieldCount)
                    {
                        UserOptions uo = new UserOptions(reader.GetName(index))
                        {
                            userText = Console.ReadLine()
                        };
                        options.Add(uo);
                        index++;
                    }
                    else return options;
                }
            }
            return options;
        }

        /// <summary>
        /// Добавить значения в таблицу
        /// </summary>
        /// <param name="tableName">Название таблицы</param>
        static void InsertToTable(string tableName)
        {
            var options = GetTableHeaders(tableName);

            QueryConstructor constructor = new QueryConstructor();
            var query = constructor.SetInsertQuery(options,tableName);

            using var newConnection = CreateConnection();
            using var cmd = new NpgsqlCommand(query, newConnection);
            constructor.SetParams(cmd, options);
            
            var status = cmd.ExecuteNonQuery().ToString();
            
            if (status == "1")
            {
                Console.WriteLine("Query is success!");
            }
            else
            {
                Console.WriteLine("Query is failed!");
            }            
        }
    }
}
