﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;

namespace SberUsers
{
    class QueryConstructor
    {

        /// <summary>
        /// Получить список заголовков таблицы
        /// </summary>
        /// <param name="tableName">Название таблицы</param>
        /// <returns>Список заголовков</returns>
        public string SetAllRowsQuery(string tableName)
        {
            return $@"SELECT * FROM public.""{tableName}""";
        }

        /// <summary>
        /// Построить запросы для всех таблиц
        /// </summary>
        /// <param name="tableNames">Список таблиц</param>
        /// <returns>Список запросов</returns>
        public List<string> SetAllRowsQuery(List<string> tableNames)
        {
            List<string> queries = new List<string>();
            if ((tableNames.Count > 0) && (tableNames != null))
            {
                tableNames.ForEach(name =>
                {
                    queries.Add($@"SELECT * FROM public.""{name}""");
                });
            } else
            {
                throw new Exception("Table names is incorrect");
            }

            return queries;
        }

        /// <summary>
        /// Установить параметры
        /// </summary>
        /// <param name="cmd">Команда</param>
        /// <param name="options">Коллекция параметров</param>
        public void SetParams(NpgsqlCommand cmd, List<UserOptions> options)
        {
            NpgsqlParameterCollection parameters = cmd.Parameters;

            options.ForEach(item =>
            {
                parameters.Add(new NpgsqlParameter(item.colHeader, int.Parse(item.userText)));
            });
        }

        public void ConvertToType(string userText)
        {
            
        }

        /// <summary>
        /// Создать запрос INSERT INTO
        /// </summary>
        /// <param name="cmd">Команда</param>
        /// <returns></returns>
        public string SetInsertQuery(List<UserOptions> options, string tableName)
        {
            string fields = string.Empty;
            string param = string.Empty;

            options.ForEach(item => {
                fields = fields + @""",""" + item.colHeader;
                param = param + ",:" + item.colHeader;
            });

            var sql = $@"INSERT INTO public.""{tableName}""({fields.Remove(0, 2)}"") VALUES({param.Remove(0, 1)});";

            return sql;
        }
    }
}
