CREATE TABLE public."Cards"
(
  "Id" integer PRIMARY KEY,
  "Card_num" integer UNIQUE NOT NULL,
  "Type" TEXT NOT NULL,
  "Pin" INTEGER NOT NULL,
  "Amount" INTEGER NOT NULL
);

CREATE TABLE public."BioInfo" (
  "INN" text PRIMARY KEY,
  "First_name" text NOT NULL,
  "Last_name" text NOT NULL,
  "Age" text NOT NULL,
  "Address" text NOT NULL
);

CREATE TABLE public."Users" (
  "Id" integer NOT NULL,
  "Login" text NOT NULL,
  "Pass" text NOT NULL,
  "Tel" text NOT NULL,
  "INN" text REFERENCES public."BioInfo" ("INN"),
  "Cards" INTEGER REFERENCES public."Cards" ("Card_num")
);

create sequence Users_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public."Users"
OWNER TO postgres;

create sequence Cards_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public."Cards"
OWNER TO postgres;